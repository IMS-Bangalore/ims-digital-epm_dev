//
//  NSData+MD5.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 01/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MD5)
- (NSString *) md5;
@end
