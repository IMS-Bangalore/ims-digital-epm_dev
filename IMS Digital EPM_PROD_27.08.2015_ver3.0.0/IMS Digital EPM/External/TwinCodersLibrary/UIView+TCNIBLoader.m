//
//  UIView+NIBLoader.m
//  YUM
//
//  Created by Guillermo Gutiérrez on 28/12/11.
//  Copyright (c) 2011 TwinCoders. All rights reserved.
//

#import "UIView+TCNIBLoader.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (TCNIBLoader)

+ (id)loadFromNIB:(NSString*)nibName silentError:(BOOL)silentError {
    id view = nil;
    // If no nib name is provided, use the classname as nib name
    if (nibName == nil) {
        nibName = NSStringFromClass([self class]);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // Check for existence of a specific iPad version of the resource
            NSString* ipadNibName = [NSString stringWithFormat:@"%@~ipad", nibName];
            NSString* path = [[NSBundle mainBundle] pathForResource:ipadNibName ofType:@"nib"];
            if (path != nil && [[NSFileManager defaultManager] fileExistsAtPath:path]) {
                nibName = ipadNibName;
            }
        }
    }
    
    NSString* path = [[NSBundle mainBundle] pathForResource:nibName ofType:@"nib"];
    if (!silentError || (path != nil && [[NSFileManager defaultManager] fileExistsAtPath:path])) {
        NSArray* elements = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        for (id element in elements) {
            if ([element isKindOfClass:[self class]]) {
                view = element;
                break;
            }
        }
    }
    else {
        TCLog(@"No NIB Found with name '%@' for class '%@'", nibName, NSStringFromClass([self class]));
    }
    
    return view;
}

+ (id)loadFromNIB:(NSString *)nibName {
    return [self loadFromNIB:nibName silentError:NO];
}

+ (id)loadFromDefaultNIB {
    return [self loadFromNIB:nil];
}

@end
