//
//  RoundedBordersView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 09/05/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCRoundedBordersView : UIView
@property (nonatomic, strong) NSNumber* cornerRadius;
@end
