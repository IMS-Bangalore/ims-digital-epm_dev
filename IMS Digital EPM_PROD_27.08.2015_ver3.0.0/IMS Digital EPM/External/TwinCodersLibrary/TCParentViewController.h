//
//  TCParentViewController.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez Doral on 10/01/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCModalAlertView.h"
#import "TCModalLoadingView.h"

#pragma mark - Landscape / Portrait protocols
@protocol LandscapeControllerProtocol <NSObject>
@optional
- (void)willPresentAlternativeLandscapeController;
- (void)willDismissAlternativeLandscapeController;
@end

@protocol PortraitControllerProtocol <NSObject>
@optional
- (void)willPresentAlternativePortraitController;
- (void)willDismissAlternativePortraitController;
@end

@protocol LandscapeViewProtocol <NSObject>
@optional
- (void)willPresentAlternativeLandscapeView;
- (void)willDismissAlternativeLandscapeView;
@end

@protocol PortraitViewProtocol <NSObject>
@optional
- (void)willPresentAlternativePortraitView;
- (void)willDismissAlternativePortraitView;
@end


#pragma mark - ParentViewController declaration
@interface TCParentViewController : UIViewController<PortraitControllerProtocol, LandscapeControllerProtocol, PortraitViewProtocol, LandscapeViewProtocol>

+ (void)setGenericModalAlertNibName:(NSString*)alertNibName;
+ (void)setGenericModalLoadingNibName:(NSString*)loadingNibName;

#pragma mark - Alternative orientation view controllers
#pragma mark - Landscape alternative View
@property (nonatomic, strong) IBOutlet UIView* landscapeView; // If set to a value other than nil, this view will be presented when the orientation changes to landscape
@property (nonatomic, readonly, getter = isShowingLandscapeView) BOOL showingLandscapeView;

#pragma mark Portrait alternative View
@property (nonatomic, strong) IBOutlet UIView* portraitView; // If set to a value other than nil, this view will be presented when the orientation changes to portrait
@property (nonatomic, readonly, getter = isShowingPortraitView) BOOL showingPortraitView;

#pragma mark - Alternative orientation views
#pragma mark Landscape alternative controller
@property (nonatomic, strong) IBOutlet UIViewController* landscapeController; // If set to a value other than nil, this view controller will be presented when the orientation changes to landscape
@property (nonatomic, readonly, getter = isShowingLandscapeController) BOOL showingLandscapeController;

#pragma mark Portrait alternative controller
@property (nonatomic, strong) IBOutlet UIViewController* portraitController; // If set to a value other than nil, this view controller will be presented when the orientation changes to portrait
@property (nonatomic, readonly, getter = isShowingPortraitController) BOOL showingPortraitController;

#pragma mark - ModalAlertView
@property (nonatomic, strong) IBOutlet TCModalLoadingView* modalLoadingView;
@property (nonatomic, strong) IBOutlet TCModalAlertView* modalAlertView;


- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
           alertAppearance:(TCAlertAppearance)alertAppearance
                     modal:(BOOL)modal
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent;

- (void)showAlertWithTitle:(NSString*)title
               description:(NSString*)description
         acceptButtonTitle:(NSString*)okButtonTitle
         cancelButtonTitle:(NSString*)cancelButtonTitle
                      icon:(UIImage*)iconImage
                     modal:(BOOL)modal
                  animated:(BOOL)animated
                   onEvent:(TCAlertEventBlock)onEvent;

- (void) showModalErrorWithMessage:(NSString*)message title:(NSString*)title buttonText:(NSString*)buttonText;
- (void) showLoadingViewWithText:(NSString*)text
                           modal:(BOOL)modal
                        animated:(BOOL)animated;
- (void) dismissLoadingViewAnimated:(BOOL)animated;

@end

