//
//  UIImage+TCSuperView.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 05/09/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UIView+TCSuperView.h"
#import <QuartzCore/QuartzCore.h>

static const NSTimeInterval kTransitionDuration = 0.3;

@implementation UIView(TCSuperView)
- (void)addSubview:(UIView *)view adjust:(BOOL)adjust animated:(BOOL)animated {
    // Cancel previous animations
    [CATransaction begin];
    [self.layer removeAllAnimations];
    [CATransaction commit];
    
    if (adjust) {
        CGSize size = self.bounds.size;
        CGRect frame = CGRectMake(0, 0, size.width, size.height);
        view.frame = frame;
    }
    
    [self addSubview:view];
    [self bringSubviewToFront:view];
    
    if (animated) {
        view.alpha = 0.0;
        [UIView transitionWithView:self duration:kTransitionDuration options:UIViewAnimationOptionCurveEaseInOut
                        animations:^{
                            view.alpha = 1.0;
                        }
                        completion:nil];
    }
}

- (void)removeFromSuperviewAnimated:(BOOL)animated {
    // Cancel previous animations
    [CATransaction begin];
    [self.layer removeAllAnimations];
    [CATransaction commit];
    
    if (animated) {
        [UIView transitionWithView:self duration:kTransitionDuration options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
            self.alpha = 1;
        }];
    }
    else {
        [self removeFromSuperview];
    }
}

@end