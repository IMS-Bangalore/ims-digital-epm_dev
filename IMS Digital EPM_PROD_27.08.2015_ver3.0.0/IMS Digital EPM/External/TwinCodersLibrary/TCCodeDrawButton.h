//
//  TCCodeDrawButton.h
//  SantanderChile
//
//  Created by Guillermo Gutiérrez on 02/11/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief Custom draw block, will be called when it's required to draw the image.
 @param codeDrawView The TCCodeDrawView that will render the image. Use this
 parameter to fetch the state and the image size if needed
 */
typedef void (^TCCodeDrawButtonBlock)(id codeDrawView);

/**
 @brief
 Parent view for views drawn by code using CoreGraphics (Quartz2D).
 Drawn image is stored in a ImageView for faster drawing on the screen.
 */
@interface TCCodeDrawButton : UIButton

#pragma mark - Properties
@property (nonatomic, readonly) CGSize imageSize;
@property (nonatomic, copy) TCCodeDrawButtonBlock drawBlock;
@property (nonatomic) BOOL redrawOnSizeChange; // Defaults to NO
@property (nonatomic) BOOL redrawOnStatusChange; // Defaults to NO
@property (nonatomic, strong) NSString* reuseIdentifier; // Reuse identifier for caching image

#pragma mark - Public methods and Override point for subclasses
/** @brief Obtains the size for the image based on the size of the */
- (CGSize)imageSizeFromViewSize:(CGSize)viewSize;

/** @brief This method is called when the image needs to be redrawn, for example
 when the image size changes */
- (void)redrawImage;
- (void)layoutImageView;

/** @brief The image identifier is used to cache drawn images for later use. */
- (NSString*)imageIdentifier;

-(void)setEnabled:(BOOL)enabled animated:(BOOL)animated;

@end