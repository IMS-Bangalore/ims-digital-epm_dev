//
//  UIView+NIBLoader.h
//  YUM
//
//  Created by Guillermo Gutiérrez on 28/12/11.
//  Copyright (c) 2011 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TCNIBLoader)

/** 
 @brief Tries to load the current view from the selected NIB
 @param nibName Name of the NIB to open to load the view. If nil, it will try to open a nib with the current class name. If current device and a ~ipad resource exists, it will try to open that resource instead.
 @param silentError if set to YES, it will silently ignore errors loading the NIB and simply return nil
 @return the first instance of the current class extracted from the specified NIB if no error occurred or nil if error occurred or instance was not found inside the NIB
 */
+ (id)loadFromNIB:(NSString*)nibName silentError:(BOOL)silentError;

/** @brief Convenience method, same as [target loadFromNIB:nibName silentError:NO] */
+ (id)loadFromNIB:(NSString*)nibName;

/** @brief Convenience method, same as [target loadFromNIB:nil] */
+ (id)loadFromDefaultNIB;
@end