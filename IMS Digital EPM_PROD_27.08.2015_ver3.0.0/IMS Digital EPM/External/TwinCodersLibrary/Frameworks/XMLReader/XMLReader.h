//
//  XMLReader.h
//
//

#import <Foundation/Foundation.h>


@interface XMLReader : NSObject<NSXMLParserDelegate>

@property (nonatomic) BOOL ignoreXMLAtributes;

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLData:(NSData *)data ignoreXMLAtributes:(BOOL)ignoreXMLAtributes error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)error withEncoding:(NSStringEncoding)encoding;
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string ignoreXMLAtributes:(BOOL)ignoreXMLAtributes error:(NSError **)error withEncoding:(NSStringEncoding)encoding;

@end
