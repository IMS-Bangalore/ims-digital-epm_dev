//
//  XMLReader.m
//

#import "XMLReader.h"

NSString *const kXMLReaderTextNodeKey = @"text";

@interface XMLReader ()
@property (nonatomic, retain) NSMutableArray *dictionaryStack;
@property (nonatomic, retain) NSMutableString *textInProgress;
@property (nonatomic, assign) NSError **errorPointer;
- (id)initWithError:(NSError **)error;
- (NSDictionary *)objectWithData:(NSData *)data;

@end


@implementation XMLReader
@synthesize dictionaryStack = dictionaryStack;
@synthesize textInProgress = textInProgress;
@synthesize errorPointer = errorPointer;

#pragma mark -
#pragma mark Public methods

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)errorPointer {
    return [self dictionaryForXMLData:data ignoreXMLAtributes:NO error:errorPointer];
}
+ (NSDictionary *)dictionaryForXMLData:(NSData *)data ignoreXMLAtributes:(BOOL)ignoreXMLAtributes error:(NSError **)errorPointer {
    XMLReader *reader = [[XMLReader alloc] initWithError:errorPointer];
    reader.ignoreXMLAtributes = ignoreXMLAtributes;
    NSDictionary *rootDictionary = [reader objectWithData:data];
    [reader release];
    return rootDictionary;
}
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)error withEncoding:(NSStringEncoding)encoding {
    return [self dictionaryForXMLString:string ignoreXMLAtributes:NO error:error withEncoding:encoding];
}
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string ignoreXMLAtributes:(BOOL)ignoreXMLAtributes error:(NSError **)error withEncoding:(NSStringEncoding)encoding {
    NSData *data = [string dataUsingEncoding:encoding];
    return [XMLReader dictionaryForXMLData:data ignoreXMLAtributes:ignoreXMLAtributes error:error];
}

#pragma mark -
#pragma mark Parsing

- (id)initWithError:(NSError **)error
{
    if (self = [super init])
    {
        errorPointer = error;
    }
    return self;
}

- (void)dealloc
{
    [dictionaryStack release];
    [textInProgress release];
    [super dealloc];
}

- (NSDictionary *)objectWithData:(NSData *)data
{
    // Clear out any old data
    [dictionaryStack release];
    [textInProgress release];
    
    dictionaryStack = [[NSMutableArray alloc] init];
    textInProgress = [[NSMutableString alloc] init];
    
    // Initialize the stack with a fresh dictionary
    [dictionaryStack addObject:[NSMutableDictionary dictionary]];
    
    // Parse the XML
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    BOOL success = [parser parse];
    [parser release];
    
    // Return the stack's root dictionary on success
    if (success)
    {
        NSDictionary *resultDict = [dictionaryStack objectAtIndex:0];
        return resultDict;
    }
    
    return nil;
}

#pragma mark -
#pragma mark NSXMLParserDelegate methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    // Get the dictionary for the current level in the stack
    NSMutableDictionary *parentDict = [dictionaryStack lastObject];

    // Create the child dictionary for the new element, and initilaize it with the attributes
    NSMutableDictionary *childDict = [NSMutableDictionary dictionary];
    
    if (!self.ignoreXMLAtributes) {
        [childDict addEntriesFromDictionary:attributeDict];
    }
    
    // If there's already an item for this key, it means we need to create an array
    id existingValue = [parentDict objectForKey:elementName];
    if (existingValue) {
        NSMutableArray *array = nil;
        if ([existingValue isKindOfClass:[NSMutableArray class]]) {
            // The array exists, so use it
            array = (NSMutableArray *) existingValue;
        } else {
            // Create an array if it doesn't exist
            array = [NSMutableArray array];
            [array addObject:existingValue];

            // Replace the child dictionary with an array of children dictionaries
            [parentDict setObject:array forKey:elementName];
        }
        
        // Add the new child dictionary to the array
        [array addObject:childDict];
    } else {
        // No existing value, so update the dictionary
        [parentDict setObject:childDict forKey:elementName];
    }
    
    // Update the stack
    [dictionaryStack addObject:childDict];
    
    // Reset the text
    [textInProgress release];
    textInProgress = [[NSMutableString alloc] init];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    // Update the parent dict with text info
    NSMutableDictionary *dictInProgress = [dictionaryStack lastObject];
    
    // Set the text property
    if ([textInProgress length] > 0 && [dictInProgress isEqualToDictionary:[NSMutableDictionary dictionary]])
    {
        if (!self.ignoreXMLAtributes) {
            [dictInProgress setObject:textInProgress forKey:kXMLReaderTextNodeKey];
        } else {
            [dictionaryStack removeLastObject];
            NSMutableDictionary* parentDict = [dictionaryStack lastObject];
            [dictionaryStack addObject:dictInProgress];
            [parentDict setObject:textInProgress forKey:elementName];
        }

        // Reset the text
        [textInProgress release];
        textInProgress = [[NSMutableString alloc] init];
    }
    
    // Pop the current dict
    [dictionaryStack removeLastObject];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    // Build the text value
    [textInProgress appendString:string];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    // Set the error pointer to the parser's error object
    *errorPointer = parseError;
}

@end
