//
//  TCCustomPageControl.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 05/09/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TCCustomPageControlDelegate;

@interface TCCustomPageControl : UIView 

// Set these to control the PageControl.
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) NSInteger numberOfPages;

// Customize these as well as the backgroundColor property.
@property (nonatomic, strong) UIColor *dotColorCurrentPage;
@property (nonatomic, strong) UIColor *dotColorOtherPage;

// Optional delegate for callbacks when user taps a page dot.
@property (nonatomic, assign) IBOutlet NSObject<TCCustomPageControlDelegate> *delegate;

@end

@protocol TCCustomPageControlDelegate<NSObject>
@optional
- (void)pageControlPageDidChange:(TCCustomPageControl *)pageControl;
@end