//
//  TCPassthroughScrollView.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 28/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCPassthroughScrollView.h"

@implementation TCPassthroughScrollView
-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (UIView *view in self.subviews) {
        if (!view.hidden && view.userInteractionEnabled && [view pointInside:[self convertPoint:point toView:view] withEvent:event])
            return YES;
    }
    return NO;
}
@end
