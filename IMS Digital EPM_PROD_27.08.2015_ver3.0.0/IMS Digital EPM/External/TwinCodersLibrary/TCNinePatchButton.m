//
//  TCNinePatchButton.m
//  FnacSocios
//
//  Created by Guillermo Gutiérrez on 13/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCNinePatchButton.h"
#import "UIImage+NinePatch.h"


@implementation TCNinePatchButton
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (( self = [super initWithCoder:aDecoder] )) {
        void (^checkImage)(UIControlState) = ^(UIControlState controlState) {
            UIImage* backgroundImage = [self backgroundImageForState:controlState];
            if (backgroundImage != nil && ![backgroundImage isStretchable]) {
                [super setBackgroundImage:[backgroundImage ninePatchImage] forState:controlState];
            }
        };
        
        // Check for Disabled, Hightlighted and Selected images
        checkImage(UIControlStateNormal);
        checkImage(UIControlStateDisabled);
        checkImage(UIControlStateHighlighted);
        checkImage(UIControlStateSelected);
    }
    return self;
}

- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state {
    [super setBackgroundImage:[image ninePatchImage] forState:state];
}
@end
