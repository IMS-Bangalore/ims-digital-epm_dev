//
//  TCHorizontalPageSelector.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 22/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCustomPageControl.h"

@class TCHorizontalPageSelector;
@protocol TCHorizontalPageSelectorDelegate <NSObject>
- (void)horizontalPageSelector:(TCHorizontalPageSelector*)pageSelector didSelectPage:(NSInteger)pageNumber;
@end

typedef enum {
    TCHorizontalPageSelectorTypeCenter = 0,
    TCHorizontalPageSelectorTypeFill
} TCHorizontalPageSelectorType;

@interface TCHorizontalPageSelector : UIView<UIScrollViewDelegate, TCCustomPageControlDelegate>
#pragma mark - Properties
@property (nonatomic, assign) IBOutlet id<TCHorizontalPageSelectorDelegate> delegate;
@property (nonatomic, strong) NSArray *views;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) TCHorizontalPageSelectorType type;

#pragma mark - IBOutlets
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet TCCustomPageControl *pageControl;

#pragma mark - Public methods
- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated;

@end
