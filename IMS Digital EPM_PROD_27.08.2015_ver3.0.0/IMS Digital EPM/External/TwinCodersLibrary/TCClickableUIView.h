//
//  TCClickableUIView.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez Doral on 26/09/11.
//  Copyright TwinCoders All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCClickableUIView : UIView

#pragma mark - IBOutlets
@property (nonatomic, strong) IBOutlet UIControl* actionButton;
@property (nonatomic, strong) IBOutletCollection(id) NSArray* actionHighlightElements;

#pragma mark - Properties
#pragma mark Action
@property (nonatomic, assign) id target;
@property (nonatomic, assign) SEL action;

#pragma mark Status
@property (nonatomic, assign, getter=isEnabled)     BOOL enabled;
@property (nonatomic, assign, getter=isHighlighted) BOOL highlighted;
@property (nonatomic, assign, getter=isSelected)    BOOL selected;

#pragma mark - Public methods
- (void)setEnabled:(BOOL)enabled animated:(BOOL)animated;
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated;

@end