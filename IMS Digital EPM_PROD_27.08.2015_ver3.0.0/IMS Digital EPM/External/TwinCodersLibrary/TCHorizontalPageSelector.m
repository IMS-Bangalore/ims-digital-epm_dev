//
//  TCHorizontalPageSelector.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 22/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCHorizontalPageSelector.h"

@implementation TCHorizontalPageSelector

#pragma mark - Init and dealloc
- (void)dealloc {
    _delegate = nil;
}

#pragma mark - Private methods
- (void)reloadUI {
    // Remove previous views
    for (UIView* view in _scrollView.subviews) {
        [view removeFromSuperview];
    }
    [self layoutIfNeeded];
    
    NSInteger currentPage = _selectedIndex;
    
    CGFloat width = _scrollView.frame.size.width;
    CGFloat height = _scrollView.frame.size.height;
    
    CGFloat offset = 0;
    
    // Add subviews
    for (UIView* view in _views) {
        CGRect viewFrame = view.frame;
        CGFloat viewWidth = viewFrame.size.width;
        CGFloat viewHeight = viewFrame.size.height;
        
        // Adjust frame depending on the type
        if (_type == TCHorizontalPageSelectorTypeFill) {
            viewFrame.size.width = width;
            viewFrame.size.height = height;
            viewFrame.origin.x = offset;
            viewFrame.origin.y = 0;
        }
        else {
            viewFrame.origin.x = offset + ((width - viewWidth) / 2);
            viewFrame.origin.y = (height - viewHeight) / 2;
        }
        view.frame = viewFrame;
        
        // Add to the scroll view
        [_scrollView addSubview:view];
        
        offset = offset + width;
    }
    
    _scrollView.contentSize = CGSizeMake(offset, 0);
    _pageControl.numberOfPages = _views.count;
    [self setSelectedIndex:currentPage animated:NO];
}

- (void)scrollToPage:(NSInteger)page animated:(BOOL)animated {
    CGFloat width = _scrollView.frame.size.width;
    CGFloat offset = width * page;
    [_scrollView setContentOffset:CGPointMake(offset, 0) animated:animated];
    _pageControl.currentPage = page;
}

#pragma mark - Public methods
- (void)setViews:(NSArray *)views {
    _views = views;

    [self setSelectedIndex:0];
    
    [self reloadUI];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    [self setSelectedIndex:selectedIndex animated:NO];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated {
    _selectedIndex = selectedIndex;
    
    [self scrollToPage:selectedIndex animated:animated];
}

#pragma mark - Overriden methods
- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self reloadUI];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _pageControl.delegate = self;
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == _scrollView) {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        if (_selectedIndex != page) {
            _selectedIndex = page;
            _pageControl.currentPage = page;
            [_delegate horizontalPageSelector:self didSelectPage:page];
        }
    }
}

#pragma mark - TCCustomPageControlDelegate
- (void)pageControlPageDidChange:(TCCustomPageControl *)pageControl {
    [self setSelectedIndex:pageControl.currentPage animated:YES];
    [_delegate horizontalPageSelector:self didSelectPage:_selectedIndex];
}

@end
