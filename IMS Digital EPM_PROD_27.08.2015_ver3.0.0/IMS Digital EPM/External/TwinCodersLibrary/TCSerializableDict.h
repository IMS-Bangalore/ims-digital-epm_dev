//
//  TCSerializableDict.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 01/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCSerializableDict : NSObject

@property(nonatomic, readonly) NSArray* serializableFieldNames;

// Initializes the object obtaining all the fields from the dictionary
- (id)initFromDictionary:(NSDictionary*)dictionary;

// Converts the object to the dictionary
- (NSDictionary*)toDictionary;

@end
