//
//  TCCheckbox.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 13/06/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCCheckbox.h"

@interface TCCheckbox()
- (void)addObserverToButtonEvents;
- (void)removeObserverForButtonEvents;
- (void)buttonClicked:(UIButton*)button;
@end

@implementation TCCheckbox

static NSString* const kDefaultImage = @"checkbox_default.png";
static NSString* const kSelectedImage = @"checkbox_selected.png";
static NSString* const kHighlightedImage = @"checkbox_highlighted.png";

#pragma mark Initialization methods

-(void) initCheckBox {
    // Set defaults
    [self setImage:[UIImage imageNamed:kDefaultImage] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:kSelectedImage] forState:UIControlStateSelected];
//    [self setImage:[UIImage imageNamed:kHighlightedImage] forState:UIControlStateHighlighted];
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self setContentMode:UIViewContentModeLeft];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [self addObserverToButtonEvents];
}

- (id)init {
    self = [super init];
    if (self) {
        [self initCheckBox];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initCheckBox];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initCheckBox];
    }
    return self;
}

- (void)dealloc {
    _checkBoxDelegate = nil;
    [self removeObserverForButtonEvents];
//    [super dealloc]; << Provided by compiler
}

- (void)addObserverToButtonEvents {
    [self addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)removeObserverForButtonEvents {
    [self removeTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];    
}

-(void)setSelected:(BOOL)selected {
    if (selected != self.selected) {
        super.selected = selected;
    }
}

#pragma mark - Button events
- (void)buttonClicked:(UIButton*)button {
    self.selected = !self.selected;
    [self.checkBoxDelegate checkBoxDidChangeValue:self];
}

@end