//
//  TCSOAPRequest.h
//  TwinCodersLibrary
//
//  Created by Alex Gutiérrez on 09/10/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "TCXMLRequest.h"

/** @brief Request that inherits TCBaseRequest to include SOAP protocol fields */
@interface TCSOAPRequest : TCXMLRequest

/** @brief Name to be included as identifier of the schema */
@property (nonatomic, strong) NSString* soapShemaName;

/** @brief URL of the Schema for soap request wrapper */
@property (nonatomic, strong) NSString* soapShemaURL;

/** @brief Name of the wrapper for the Soap content*/
@property (nonatomic, strong) NSString* soapContentWrapper;

/** @brief Pattern for the request URL. Must accept a String parameter to include the endPoint */
@property (nonatomic, strong) NSString *soapUrlPattern;

/** @brief Endpoint for soap url */
@property (nonatomic, strong) NSString* soapEndpoint;

/** @brief Soap action */
@property (nonatomic, strong) NSString* soapAction;

/** @brief Defines if the launcher have to ignore the request schema for the content wrapper */
@property (nonatomic) BOOL ignoreSchema;

@end