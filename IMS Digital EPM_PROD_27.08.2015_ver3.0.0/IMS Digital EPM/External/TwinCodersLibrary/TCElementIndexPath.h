//
//  TCPageIndexPath.h
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 16/01/13.
//  Copyright (c) 2013 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCElementIndexPath : NSObject<NSCopying>
@property (nonatomic, assign) NSInteger elementIndex;
@property (nonatomic, assign) NSInteger contentIndex;

- (id)initWithContentIndex:(NSInteger)contentIndex elementIndex:(NSInteger)elementIndex;
+ (id)elementIndexWithContentIndex:(NSInteger)contentIndex elementIndex:(NSInteger)elementIndex;

@end
