//
//  TCAlertView.m
//  TwinCodersLibrary
//
//  Created by Guillermo Gutiérrez on 07/09/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "TCAlertView.h"

@interface TCAlertView()
@property (nonatomic, copy) TCAlertCancelBlock cancelBlock;
@property (nonatomic, copy) TCAlertDismissBlock dismissBlock;
@end

@implementation TCAlertView

+ (id) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) otherButtons
                          onDismiss:(TCAlertDismissBlock) dismissed
                           onCancel:(TCAlertCancelBlock) cancelled {
    
    TCAlertView *alert = [[TCAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:[self class]
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:nil];
    
    [alert setDismissBlock:dismissed];
    [alert setCancelBlock:cancelled];
    
    for (NSString *buttonTitle in otherButtons) {
        [alert addButtonWithTitle:buttonTitle];
    }
    
    return alert;
}

+ (id) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message {
    
    return [TCAlertView alertViewWithTitle:title
                                   message:message
                         cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")];
}

+ (id) alertViewWithTitle:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle {
    TCAlertView *alert = [[TCAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles: nil];
    return alert;
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex {
    
	if (buttonIndex == [alertView cancelButtonIndex]) {
		if (self.cancelBlock) {
            self.cancelBlock();
        }
	}
    else {
        if (self.dismissBlock) {
            self.dismissBlock(buttonIndex - 1); // cancel button is button 0
        }
    }
}

@end
