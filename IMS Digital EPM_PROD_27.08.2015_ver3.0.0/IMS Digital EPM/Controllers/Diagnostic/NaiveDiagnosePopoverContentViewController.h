//
//  UIWeekdaySelectionPopoverContentViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 01/12/12.
//
//

#import <UIKit/UIKit.h>

@protocol NaiveDiagnosePopoverContentViewControllerDelegate <NSObject>

- (void)didSelectNaive:(BOOL)naive;

@end

@interface NaiveDiagnosePopoverContentViewController : UIViewController

#pragma mark Properties
@property (assign, nonatomic) BOOL naive;
@property (assign, nonatomic) id<NaiveDiagnosePopoverContentViewControllerDelegate> delegate;

@end
