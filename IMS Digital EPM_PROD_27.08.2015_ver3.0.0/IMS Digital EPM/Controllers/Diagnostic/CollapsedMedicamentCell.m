//
//  CollapsedMedicamentCell.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 11/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "CollapsedMedicamentCell.h"

@implementation CollapsedMedicamentCell
@synthesize cellTitle = _cellTitle;
@synthesize deleteMedicamentButton = _deleteMedicamentButton;
@synthesize expandButton = _expandButton;
@synthesize delegate = _delegate;
@synthesize enabled = _enabled;

#pragma mark - Init and dealloc

- (void)dealloc {
    [_cellTitle release];
    [_deleteMedicamentButton release];
    [_expandButton release];
    [_incompleteMedicamentWarningImageView release];
    [super dealloc];
}

#pragma mark - Private methods

#pragma mark - Public methods

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellTitle:(NSString *)cellTitle {
    [cellTitle retain];
    [_cellTitle release];
    _cellTitle = cellTitle;
    
    [_expandButton setTitle:cellTitle forState:UIControlStateNormal];
}

- (void)setEnabled:(BOOL)flag {
    _enabled = flag;
    _deleteMedicamentButton.enabled = flag;
    _expandButton.enabled = flag;
}

- (void)hideIncompleteTreatmentAlert:(BOOL)hide {
    self.incompleteMedicamentWarningImageView.hidden = hide;
}

- (IBAction)deleteMedicamentButtonClicked:(id)sender {
    [_delegate collapsedMedicamentCellDeleteButtonClicked:self];
}

- (IBAction)expandButtonClicked:(id)sender {
    [_delegate collapsedMedicamentCellExpandButtonClicked:self];
}

@end
