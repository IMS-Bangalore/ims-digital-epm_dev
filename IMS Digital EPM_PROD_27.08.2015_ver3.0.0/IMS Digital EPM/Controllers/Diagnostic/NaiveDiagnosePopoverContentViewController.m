//
//  UIWeekdaySelectionPopoverContentViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 01/12/12.
//
//

#import "NaiveDiagnosePopoverContentViewController.h"
#import "CheckBox.h"

@interface NaiveDiagnosePopoverContentViewController () <CheckBoxDelegate>

#pragma mark IBOutlets
@property (retain, nonatomic) IBOutlet UIView *containerView;
@property (retain, nonatomic) IBOutlet CheckBox *checkBox;

@end

@implementation NaiveDiagnosePopoverContentViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    frame.size = self.containerView.frame.size;
    self.view.frame = frame;

    self.checkBox.checkBoxDelegate = self;
    self.checkBox.selected = self.naive;
}

- (void)dealloc {
    [_containerView release];
    [_checkBox release];
    [super dealloc];
}

- (void)viewDidUnload {
    self.containerView = nil;
    self.checkBox = nil;
    [super viewDidUnload];
}

#pragma mark - WeekdaySelectionViewDelegate

-(void)checkBoxDidChangeValue:(CheckBox *)checkBox {
    [self.delegate didSelectNaive:checkBox.selected];
}

@end
