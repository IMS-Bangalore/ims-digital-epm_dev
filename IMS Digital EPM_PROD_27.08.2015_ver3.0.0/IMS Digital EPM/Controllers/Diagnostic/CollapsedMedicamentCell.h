//
//  CollapsedMedicamentCell.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 11/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Medicament.h"

@class CollapsedMedicamentCell;
@protocol CollapsedMedicamentCellDelegate <NSObject>
- (void)collapsedMedicamentCellExpandButtonClicked:(CollapsedMedicamentCell*)cell;
- (void)collapsedMedicamentCellDeleteButtonClicked:(CollapsedMedicamentCell*)cell;
@end

@interface CollapsedMedicamentCell : UITableViewCell

#pragma mark - Properties
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (retain, nonatomic) NSString* cellTitle;
@property (assign, nonatomic) id<CollapsedMedicamentCellDelegate> delegate;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UIButton *deleteMedicamentButton;
@property (retain, nonatomic) IBOutlet UIButton *expandButton;
@property (retain, nonatomic) IBOutlet UIImageView *incompleteMedicamentWarningImageView;

#pragma mark - IBActions
- (IBAction)deleteMedicamentButtonClicked:(id)sender;
- (IBAction)expandButtonClicked:(id)sender;
- (void)hideIncompleteTreatmentAlert:(BOOL)hide;


@end
