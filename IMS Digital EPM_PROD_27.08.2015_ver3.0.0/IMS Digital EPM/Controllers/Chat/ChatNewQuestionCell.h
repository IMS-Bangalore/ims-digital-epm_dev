//
//  ChatNewQuestionCell.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 10/12/12.
//
//

#import <UIKit/UIKit.h>

@protocol ChatNewQuestionCellDelegate <NSObject>

- (void)openMessageComposer;

@end

@interface ChatNewQuestionCell : UITableViewCell

#pragma mark - Properties
@property (nonatomic, assign) id<ChatNewQuestionCellDelegate> delegate;

//Ravi_Bukka: Added for localization
@property (nonatomic, retain) IBOutlet UILabel *questionLabel;

#pragma mark - IBActions
- (IBAction)openQuestionComposer:(id)sender;

@end
