//
//  ChatMessageComposerViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "ChatMessageComposerViewController.h"
#import "RequestFactory.h"
#import "BaseRequest.h"

@interface ChatMessageComposerViewController ()

@property (nonatomic, retain) RequestFactory* requestFactory;
@property (nonatomic, retain) BaseRequest* request;
@property (nonatomic, retain) MultipleChoiceViewController* choiceController;

@end

@implementation ChatMessageComposerViewController

@synthesize userId = _userId;
@synthesize message = _message;
@synthesize delegate = _delegate;
@synthesize requestFactory = _requestFactory;
@synthesize request = _request;
@synthesize choiceController = _choiceController;
@synthesize cancelButton = _cancelButton;
@synthesize sendButton = _sendButton;

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Public methods

- (void)removeKeyboard {
    [self.textView resignFirstResponder];
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL",nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL",nil) forState:UIControlStateHighlighted];
    
    
    [self.sendButton setTitle:NSLocalizedString(@"SEND",nil) forState:UIControlStateNormal];
    [self.sendButton setTitle:NSLocalizedString(@"SEND",nil) forState:UIControlStateHighlighted];
    
    
//Ravi_Bukka: Added to change the button title text color to white
    self.cancelButton.titleLabel.textColor = [UIColor whiteColor];
    self.sendButton.titleLabel.textColor = [UIColor whiteColor];
    
    [self enableButton:NO];
    self.textView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_textView release];
    [_sendMessageButton release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTextView:nil];
    [self setSendMessageButton:nil];
    [super viewDidUnload];
}

#pragma mark - Private methods

- (void)enableButton:(BOOL)enable {
    self.sendMessageButton.userInteractionEnabled = enable;
    if (enable) {
        self.sendMessageButton.alpha = 1;
    } else {
        self.sendMessageButton.alpha = 0.5;
    }
}

- (void)displayAlert:(NSString*)message {
    if (self.choiceController != nil) {
        [_delegate dismissModalController:self.choiceController];
    }
    self.choiceController = [[MultipleChoiceViewController new] autorelease];
    _choiceController.message = message;
    _choiceController.messageFont = [UIFont boldSystemFontOfSize:32];
    _choiceController.acceptButtonTitle = nil;
    _choiceController.cancelButtonTitle = NSLocalizedString(@"CHANGE_PASSWORD_ERROR_BUTTON", @"");
    _choiceController.otherButtonTitle = nil;
    _choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [_delegate dismissModalController:self.choiceController];
        self.choiceController = nil;
    };
    
    // Present modal controller
    [_delegate presentModalController:self.choiceController];
}

#pragma mark - UITextViewDelegate

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    BOOL shouldChange = NO;
    NSString* introducedText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    self.message = introducedText;
    self.textView.text = introducedText;
    [self enableButton:![introducedText isEqualToString:@""]];
    return shouldChange;
}

#pragma mark - IBActions

- (IBAction)cancel:(id)sender {
    [self.delegate cancelComposer];
}

- (IBAction)sendMessage:(id)sender {
    [self enableButton:NO];
    ChatMessage* chatMessage = [[ChatMessage alloc] init];
    chatMessage.date = [NSDate date];
    chatMessage.message = _textView.text;
    chatMessage.question = YES;
    self.requestFactory = [RequestFactory sharedInstance];
    self.request = [self.requestFactory createChatSendQuestionRequestWithUserId:self.userId andChatMessage:chatMessage onComplete:^{
        [self.delegate messageSended:chatMessage];
    } onError:^(NSError *error) {
        [self displayAlert:error.localizedDescription];
    }];
    [self.request start];
}

@end
