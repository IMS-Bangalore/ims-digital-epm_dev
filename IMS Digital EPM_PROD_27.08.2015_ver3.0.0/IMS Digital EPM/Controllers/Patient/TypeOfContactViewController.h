//
//  TypeOfContactViewController.h
//  IMS Digital ES
//
//  Created by Daniel Deneka on 06/05/2020.
//

#import <UIKit/UIKit.h>
#import "Patient.h"

@protocol TypeOfContactViewControllerDelegate <NSObject>

- (void)didSelectTypeOfContact:(TypeOfContact)typeOfContact;

@end

@interface TypeOfContactViewController : UIViewController

#pragma mark Properties
@property (assign, nonatomic) TypeOfContact typeOfContact;
@property (assign, nonatomic) id<TypeOfContactViewControllerDelegate> delegate;

@end
