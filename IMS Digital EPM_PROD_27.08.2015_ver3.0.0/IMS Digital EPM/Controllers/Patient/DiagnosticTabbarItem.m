//
//  DiagnosticTabbarItem.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 11/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "DiagnosticTabbarItem.h"

@implementation DiagnosticTabbarItem
@synthesize button = _button;
@synthesize closeButton = _closeButton;
@synthesize index = _index;
@synthesize delegate = _delegate;
@synthesize title = _title;

#pragma mark Initialization

-(void)awakeFromNib {
     _button.titleLabel.adjustsFontSizeToFitWidth = YES;
}

- (BOOL)isSelected {
    return !_button.selected;
}
- (void)setSelected:(BOOL)flag {
    _button.selected = !flag;
    _closeButton.hidden = !flag;
}

- (BOOL)isEnabled {
    return _button.enabled;
}
- (void)setEnabled:(BOOL)enabled {
    _button.enabled = enabled;
    _closeButton.enabled = enabled;
}

- (NSString *)title {
    return [[_title retain] autorelease]; 
}

- (void)setTitle:(NSString *)aTitle {
    if (_title != aTitle) {
        [aTitle retain];
        [_title release];
        _title = aTitle;
        [_button setTitle:_title forState:UIControlStateNormal];
    }
}

- (void)hideIncompleteDiagnosisAlert:(BOOL)hide {
    self.incompleteDiagnosisAlertImageView.hidden = hide;
}

- (void)dealloc {
    [_button release];
    [_closeButton release];
    [_title release];
    [_incompleteDiagnosisAlertImageView release];
    [super dealloc];
}

- (IBAction)buttonClicked:(id)sender {
    [_delegate tabbarItemClicked:self];
}

- (IBAction)closeButtonClicked:(id)sender {
    [_delegate tabbarItemCloseButtonClicked:self];
}
@end
