//
//  WeekdayButton.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 08/10/12.
//
//

#import <UIKit/UIKit.h>
#import "TwinCodersLibrary.h"

@protocol WeekdayButtonDelegate <NSObject>

- (void)dateSelected:(NSInteger)weekDay;

@end

@interface WeekdayButton : TCClickableUIView

@property (retain, nonatomic) NSDate* date;

@property (nonatomic, assign) id<WeekdayButtonDelegate> delegate;
@property (retain, nonatomic) IBOutlet UILabel *labelDay;
@property (retain, nonatomic) IBOutlet UILabel *labelDayNumber;

- (void)buttonClicked;

@end
