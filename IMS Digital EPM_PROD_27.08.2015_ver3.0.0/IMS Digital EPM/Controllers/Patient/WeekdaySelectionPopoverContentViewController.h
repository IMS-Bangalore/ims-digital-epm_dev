//
//  UIWeekdaySelectionPopoverContentViewController.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 01/12/12.
//
//

#import <UIKit/UIKit.h>
#import "WeekdaySelectionView.h"

@protocol UIWeekdaySelectionPopoverContentViewControllerDelegate <NSObject>

- (void)selectVisitDay:(NSDate*)date;

@end

@interface WeekdaySelectionPopoverContentViewController : UIViewController <WeekdaySelectionViewDelegate>

#pragma mark Properties
@property (retain, nonatomic) NSDate* firstDate;
@property (retain, nonatomic) NSDate* secondDate;
@property (assign, nonatomic) id<UIWeekdaySelectionPopoverContentViewControllerDelegate> delegate;

#pragma mark IBOutlets
@property (retain, nonatomic) IBOutlet UIView *firstWeekView;
@property (retain, nonatomic) IBOutlet UIView *secondWeekView;
@property (retain, nonatomic) IBOutlet UIView *containerView;

//Ravi_Bukka: Added for localization
@property (retain, nonatomic) IBOutlet UILabel *firstWeekColaborationLabel;
@property (retain, nonatomic) IBOutlet UILabel *secondWeekColaborationLabel;


@end
