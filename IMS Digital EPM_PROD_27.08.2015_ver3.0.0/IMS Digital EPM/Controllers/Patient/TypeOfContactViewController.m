//
//  TypeOfContactViewController.m
//  IMS Digital ES
//
//  Created by Daniel Deneka on 06/05/2020.
//

#import "TypeOfContactViewController.h"
#import "NaiveDiagnosePopoverContentViewController.h"
#import "CustomSegmentedControl.h"
#import "CheckBox.h"

@interface TypeOfContactViewController ()

@property (retain, nonatomic) IBOutlet UIView *containerView;
@property (retain, nonatomic) IBOutlet CustomSegmentedControl *typeOfContactSelector;

@end

@implementation TypeOfContactViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    CGRect frame = self.view.frame;
    frame.size = self.containerView.frame.size;
    self.view.frame = frame;

    if (self.typeOfContact > TypeOfContactUndefined && self.typeOfContact < TypeOfContactCount) {
        self.typeOfContactSelector.selectedSegmentIndex = self.typeOfContact - 1;
    }
}

- (void)dealloc {
    [_containerView release];
    [_typeOfContactSelector release];
    [super dealloc];
}

- (void)viewDidUnload {
    self.containerView = nil;
    self.typeOfContactSelector = nil;
    [super viewDidUnload];
}

#pragma mark - delegate

- (IBAction)typeOfContactValueChanged:(id)sender {
    if (self.typeOfContactSelector.selectedSegmentIndex == UISegmentedControlNoSegment) {
        [self.delegate didSelectTypeOfContact:TypeOfContactUndefined];
    } else if (self.typeOfContactSelector.selectedSegmentIndex >= 0 && self.typeOfContactSelector.selectedSegmentIndex < (TypeOfContactCount - 1)) {
        TypeOfContact typeOfContact = self.typeOfContactSelector.selectedSegmentIndex + 1;
        [self.delegate didSelectTypeOfContact:typeOfContact];
    } else {
        [self.delegate didSelectTypeOfContact:TypeOfContactUndefined];
    }
}

@end
