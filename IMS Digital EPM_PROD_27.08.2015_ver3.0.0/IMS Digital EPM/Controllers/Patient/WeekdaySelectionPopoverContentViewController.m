//
//  UIWeekdaySelectionPopoverContentViewController.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 01/12/12.
//
//

#import "WeekdaySelectionPopoverContentViewController.h"

@interface WeekdaySelectionPopoverContentViewController ()

@end

@implementation WeekdaySelectionPopoverContentViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    frame.size = self.containerView.frame.size;
    self.view.frame = frame;
    
    // Do any additional setup after loading the view from its nib.
    [self addWeekViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    _firstWeekColaborationLabel.text = NSLocalizedString(@"DOCTOR_FIRSTWEEK_COLLABORATION", nil);
    _secondWeekColaborationLabel.text = NSLocalizedString(@"DOCTOR_SECONDWEEK_COLLABORATION", nil);
    
    _firstWeekColaborationLabel.textColor = [UIColor whiteColor];
    _secondWeekColaborationLabel.textColor = [UIColor whiteColor];
    

}

- (void)dealloc {
    [_firstWeekView release];
    [_secondWeekView release];
    [_containerView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setFirstWeekView:nil];
    [self setSecondWeekView:nil];
    [self setContainerView:nil];
    [super viewDidUnload];
}

#pragma mark Private methods

- (void)addWeekViews {
    WeekdaySelectionView* firstWeekdaySelectionView = [WeekdaySelectionView loadFromDefaultNIB];
    firstWeekdaySelectionView.delegate = self;
    
    WeekdaySelectionView* secondWeekdaySelectionView = [WeekdaySelectionView loadFromDefaultNIB];
    secondWeekdaySelectionView.delegate = self;
    
    if ([_firstDate compare:_secondDate] == NSOrderedDescending) {
        firstWeekdaySelectionView.firstDate = _secondDate;
        secondWeekdaySelectionView.firstDate = _firstDate;
    } else {
        firstWeekdaySelectionView.firstDate = _firstDate;
        secondWeekdaySelectionView.firstDate = _secondDate;
    }
    
    [self.firstWeekView addSubview:firstWeekdaySelectionView];
    [self.secondWeekView addSubview:secondWeekdaySelectionView];
    
}

#pragma mark - WeekdaySelectionViewDelegate

- (void)selectVisitDay:(NSDate *)date {
    [self.delegate selectVisitDay:date];
}

@end
