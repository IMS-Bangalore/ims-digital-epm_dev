//
//  TherapyTypeReplacementReasonViewController.h
//  IMS Digital ES
//
//  Created by Daniel Deneka on 27/11/2018.
//

#import <UIKit/UIKit.h>

@interface TherapyTypeReplacementReasonViewController : UIViewController

@property (retain, nonatomic, readonly) IBOutlet UITextView *textView;

@end
