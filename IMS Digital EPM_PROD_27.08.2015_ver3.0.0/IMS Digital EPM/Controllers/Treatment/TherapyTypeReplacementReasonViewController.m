//
//  TherapyTypeReplacementReasonViewController.m
//  IMS Digital ES
//
//  Created by Daniel Deneka on 27/11/2018.
//

#import "TherapyTypeReplacementReasonViewController.h"

@interface TherapyTypeReplacementReasonViewController ()

@property (retain, nonatomic) IBOutlet UIView *containerView;
@property (retain, nonatomic) IBOutlet UITextView *textView;

@end

@implementation TherapyTypeReplacementReasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    frame.size = self.containerView.frame.size;
    self.view.frame = frame;
}

@end
