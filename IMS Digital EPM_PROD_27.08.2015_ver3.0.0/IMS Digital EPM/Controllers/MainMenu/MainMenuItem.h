//
//  MainMenuItem.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/10/12.
//
//

#import <Foundation/Foundation.h>

@interface MainMenuItem : NSObject
@property (nonatomic, retain) NSString* title;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic) NSInteger badgeCount;

@end
