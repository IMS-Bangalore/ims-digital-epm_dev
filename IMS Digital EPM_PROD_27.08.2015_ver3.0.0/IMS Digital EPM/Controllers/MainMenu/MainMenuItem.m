//
//  MainMenuItem.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/10/12.
//
//

#import "MainMenuItem.h"

@implementation MainMenuItem
@synthesize title = _title;
@synthesize identifier = _identifier;
@synthesize badgeCount = _badgeCount;

#pragma mark - Init and dealloc
- (void)dealloc {
    [_title release];
    [super dealloc];
}



@end
