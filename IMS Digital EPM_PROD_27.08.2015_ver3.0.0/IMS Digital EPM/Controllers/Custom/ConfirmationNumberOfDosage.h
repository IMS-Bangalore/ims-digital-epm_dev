//
//  ConfirmationNumberOfDosage.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 25/11/14.
//
//

#import <UIKit/UIKit.h>

@protocol ConfirmationNumberOfDosageDelegate
- (void)didClickConfirmationButton:(BOOL)accepted;
@end

typedef void (^ConfirmationButtonEventBlock)(BOOL accepted);

@interface ConfirmationNumberOfDosage : UIViewController


#pragma mark - Public methods
- (id)initWithMessage:(NSString*)message eventBlock:(ConfirmationButtonEventBlock)eventBlock;
- (id)initWithMessage:(NSString*)message delegate:(id<ConfirmationNumberOfDosageDelegate>)delegate;

#pragma mark - Properties
@property (nonatomic, retain) NSString* message;
@property (nonatomic, retain) NSString* acceptButtonTitle;
@property (nonatomic, retain) NSString* cancelButtonTitle;
@property (nonatomic, assign) id<ConfirmationNumberOfDosageDelegate> delegate;
@property (nonatomic, copy) ConfirmationButtonEventBlock eventBlock;

#pragma mark - IBActions
- (IBAction)acceptButtonClicked:(id)sender;
- (IBAction)cancelButtonClicked:(id)sender;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@property (retain, nonatomic) IBOutlet UIButton *acceptButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;

@end
