//
//  ComponentGroup.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 08/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "ComponentGroup.h"

const CGFloat kComponentDisabledAlpha = 0.5;

@implementation ComponentGroup

@synthesize enabled = _enabled;
@synthesize components = _components;
@synthesize enabledBackgroundColor = _enabledBackgroundColor;
@synthesize disabledBackgroundColor = _disabledBackgroundColor;

#pragma mark Initialization methods

-(void) initialization {
    // Set defaults
    self.backgroundColor = self.enabledBackgroundColor = [UIColor colorWithRed:29/255.0 green:174/255.0 blue:236/255.0 alpha:1];
    self.disabledBackgroundColor = [UIColor colorWithRed:176/255.0 green:176/255.0 blue:176/255.0 alpha:1];
    _enabled = YES;
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialization];
    }
    return self;
}

- (void)dealloc {
    [_enabledBackgroundColor release];
    [_disabledBackgroundColor release];
    [_components release];
    [super dealloc];
}

-(void)awakeFromNib {
    self.enabled = self.enabled;
}

-(void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    
    self.backgroundColor = enabled ? _enabledBackgroundColor : _disabledBackgroundColor;
    
    for (id component in self.components) {
        // Disable components that respond to 'setEnabled'
        if ([component respondsToSelector:@selector(setEnabled:)]) {
            [component setEnabled:enabled];
        }
    }
}

@end
