//
//  CustonTextField.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "CustomTextField.h"
#import "ComponentGroup.h"

@implementation CustomTextField
@synthesize displayError = _displayError;


#pragma mark Initialization methods

-(void) initialization {
    // Set defaults
    [self setBorderStyle:UITextBorderStyleBezel];
    [self setFont:[UIFont systemFontOfSize:14]];
    self.displayError = NO;
}

- (id)init {
    self = [super init];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialization];
    }
    return self;
}

-(void)setEnabled:(BOOL)enabled {
    super.enabled = enabled;
    self.alpha = enabled ? 1 : kComponentDisabledAlpha;
}

- (void)setDisplayError:(BOOL)displayError {
    _displayError = displayError;
    if (displayError) {
        [self setBackground:[UIImage imageNamed:@"text_field_error.png"]];
    }
    else {
        [self setBackground:[UIImage imageNamed:@"text_field.png"]];
    }
}

@end
