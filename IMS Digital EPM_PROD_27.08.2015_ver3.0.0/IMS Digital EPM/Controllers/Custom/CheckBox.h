//
//  CheckBox.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 08/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CheckBox;

@protocol CheckBoxDelegate <NSObject>

- (void)checkBoxDidChangeValue:(CheckBox *)checkBox;

@optional
- (void)checkBoxDidReselectValue:(CheckBox *)checkBox;

@end

@interface CheckBox : UIButton

@property (nonatomic, retain) id value;
@property (nonatomic, assign) IBOutlet id<CheckBoxDelegate> checkBoxDelegate;

@end
