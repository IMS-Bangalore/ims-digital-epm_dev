//
//  CustomPickerView.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 07/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPickerView : UIView <UIPickerViewDelegate>
{
    CGFloat fontSize;
}
@property (readonly) UIPickerView *pickerView;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign) IBOutlet id<UIPickerViewDelegate> delegate;
@property (nonatomic, assign) IBOutlet id<UIPickerViewDataSource> dataSource;
@property (nonatomic, retain) NSString *overlappedText;
@property (nonatomic, retain) UIFont *overlappedTextFont;





@end
