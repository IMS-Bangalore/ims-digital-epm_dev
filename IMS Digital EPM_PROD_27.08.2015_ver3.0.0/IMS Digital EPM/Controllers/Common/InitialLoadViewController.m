//
//  InitialLoadViewController.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 12/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "InitialLoadViewController.h"
#import "MultipleChoiceViewController.h"
#import "EntityFactory.h"

static CGFloat processStep = 1.0 / 21.0;

@interface InitialLoadViewController()
@property (nonatomic, assign) NSNumber* currentProgress;
@end

@implementation InitialLoadViewController
@synthesize progressView;
@synthesize delegate = _delegate;
@synthesize currentProgress = _currentProgress;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)setCurrentProgress:(NSNumber*)currentProgress {
    [currentProgress retain];
    [_currentProgress release];
    _currentProgress = currentProgress;
    if ([self.progressView respondsToSelector:@selector(setProgress:animated:)]) {
        [self.progressView setProgress:currentProgress.floatValue animated:YES];
    }
    else {
        self.progressView.progress = currentProgress.floatValue;
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setProgressView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc {
    [progressView release];
    _delegate = nil;
    [super dealloc];
}

#pragma mark - InitialLoadProcessorDelegate
- (void)initialLoadProcessorDidBeginProcessing:(NSString*)entityName {
    
}

- (void)initialLoadProcessorDidEndProcessing:(NSString *)entityName {
    NSNumber* process = [NSNumber numberWithFloat:_currentProgress.floatValue + processStep];
    [self performSelectorOnMainThread:@selector(setCurrentProgress:) withObject:process waitUntilDone:NO];
    DLog(@"Finished processing %@", entityName);
}

- (void)initialLoadProcessorDidBeginLoading {
    self.currentProgress = [NSNumber numberWithFloat:0.0];
}

- (void)initialLoadProcessorDidFinishLoading {
    self.currentProgress = [NSNumber numberWithFloat:1.0];
    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate initialLoadViewControllerDidFinishLoading];
    });
}

- (void)initialLoadProcessorDidFailWithError:(NSError*)error {
    MultipleChoiceViewController *choiceController = [[MultipleChoiceViewController new] autorelease];
    choiceController.message = NSLocalizedString(@"INITIAL_LOAD_ERROR", @"");
    choiceController.acceptButtonTitle = NSLocalizedString(@"INITIAL_LOAD_ERROR_ACCEPT", @"");
    choiceController.eventBlock = ^(MultipleChoiceButton button){
        // Dismiss and release the controller
        [_delegate dismissModalController:choiceController];
        
        switch (button) {
            case kMultipleChoiceAccept: {
                // Borramos la base de datos y la creamos de nuevo
                [[EntityFactory sharedEntityFactory] deleteDatabase];
                [self performInitialLoad];
                break;
            }
            default: {
                break;
            }
        }
    };
    
    // Present modal controller
    [_delegate presentModalController:choiceController];
}

- (void)performInitialLoadOnCurrentThread {
    @autoreleasepool {
        // Pre-load initial data if needed
        InitialLoadProcessor* ilp = [[InitialLoadProcessor alloc] init];
        [ilp preLoadData:self];
        
        [ilp release];
    }
}

- (void)performInitialLoad {
    [self performSelectorInBackground:@selector(performInitialLoadOnCurrentThread) withObject:nil];
}

@end
