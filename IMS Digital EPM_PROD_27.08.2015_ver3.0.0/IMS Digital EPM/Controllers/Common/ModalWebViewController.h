//
//  ModalWebViewController.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 15/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModalWebViewController;
@protocol ModalWebViewControllerDelegate <NSObject>

-(void) modalWebViewControllerRequestClose:(ModalWebViewController*)controller;

@end


@interface ModalWebViewController : UIViewController<UIWebViewDelegate, UIScrollViewDelegate>

@property (nonatomic, assign) id<ModalWebViewControllerDelegate> delegate;

@property (retain, nonatomic) IBOutlet UIView *popupView;
@property (retain, nonatomic) IBOutlet UIView *webViewHolderView;
@property (retain, nonatomic) IBOutlet UIWebView *contentWebView;
@property (retain, nonatomic) NSURL* url;

@property (retain, nonatomic) IBOutlet UIButton *returnButton;

- (IBAction)backButtonClicked:(id)sender;
- (void)closeModal;

@end
