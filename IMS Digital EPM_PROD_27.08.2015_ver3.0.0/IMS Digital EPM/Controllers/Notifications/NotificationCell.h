//
//  NotificationCell.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 12/12/12.
//
//

#import <UIKit/UIKit.h>
#import "Notification.h"

@interface NotificationCell : UITableViewCell

#pragma mark - Properties
@property (retain, nonatomic) Notification* notification;
@property (nonatomic) BOOL highlightOnSelected;
@property (retain, nonatomic) IBOutletCollection(UIView) NSArray *highlightElements;

#pragma mark - IBOutlets
@property (retain, nonatomic) IBOutlet UILabel *notificationDateLabel;
@property (retain, nonatomic) IBOutlet UILabel *notificationDescriptionLabel;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (retain, nonatomic) IBOutlet UIImageView *selectedImageView;

#pragma mark - Public methods
- (void)setLoading:(BOOL)loading;

@end
