//
//  BaseRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
//Ravi_Bukka: CR#2: Patient Difference Local Notification
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

extern NSString* const kRequestErrorCodeKey;
extern NSString* const kBaseRequestNetworkErrorDomain;
extern NSString* const kBaseRequestErrorDomain;

@class BaseRequest;
@class RequestLauncher;

/** @brief Protocol that must be implemented by objects that want to be notified when a Request ends, regardless of its result */
@protocol RequestEndDelegate <NSObject>

/** @brief Notifies that a request did end, regardless of its result */ 
-(void) requestDidFinish:(BaseRequest*)aRequest;

@end

/** @brief Block that will be given for objects that will handle request response, in order to handle request errors */
typedef void(^RequestErrorBlock)(NSError* error);

/**
 @brief Method that is called when the request receives a response from server
 @param response Diccionary containing request response
 */
typedef void(^RequestCompleteBlock)(NSDictionary* response);

/**
 @brief Block to be called when a request is completed successfuly without response data */
typedef void(^RequestSuccessBlock)();

/** @brief Abstract petition that defines common properties for all the application requests */
@interface BaseRequest : NSObject {
    /** Array of end delegates that will be notified when the request finishes */
    NSMutableArray* _endDelegateArray;
}

/** @brief Unique identifier for Request */
@property (nonatomic, retain, readonly) NSString* requestId;

/** @brief Name of the request */
@property (nonatomic, retain) NSString *name;

/** @brief Block that will be executed if the request returns an error */
@property (nonatomic, copy) RequestErrorBlock onError;

/** @brief Block that will be executed when request successfuly completes */
@property (nonatomic, copy) RequestCompleteBlock onComplete;

/** @brief Key of the service method that will be called */
@property (nonatomic, retain) NSString *requestMethodKey;

/** @brief Parameters of the request */
@property (nonatomic, retain) NSMutableArray* contentParams;

/** @brief Property that indicates whether the request has been canceled */
@property bool canceled;

/** @brief Reference to Request Launcher */
@property (nonatomic, assign) RequestLauncher *requestLauncher;

/** @brief Starts the asynchronous execution of the request */
-(void)start;

/** @brief Cancels the request if it has not yet received a response */
-(void)cancel;

/** @brief Add a delegate that receives notification that the request is completed, regardless of the outcome of the same */
-(void)addRequestEndDelegate:(NSObject<RequestEndDelegate>*)endDelegate;

/** @brief Removes a request end delegate */
-(void)removeRequestEndDelegate:(NSObject<RequestEndDelegate>*)endDelegate;

/** @brief Method that adds a param with given properties to content params */
-(void)addParam:(NSString*)paramValue forKey:(NSString*)paramKey;

/** @brief Method that adds a param with given properties to content params with a default value if the paramValue is nil*/
-(void)addParam:(NSString*)paramValue forKey:(NSString*)paramKey defaultValue:(NSString*)defaultValue;

/** @brief Method that adds a numeric param to content params */
-(void)addNumberParam:(NSNumber*)paramValue forKey:(NSString*)paramKey;

/** @brief Method that adds a numeric param to content params with a default value if the paramValue is nil*/
-(void)addNumberParam:(NSNumber*)paramValue forKey:(NSString*)paramKey defaultValue:(NSString*)defaultValue;

//Ravi_Bukka: CR#2: Patient Difference and Local Notification
- (BOOL)connected;
@end