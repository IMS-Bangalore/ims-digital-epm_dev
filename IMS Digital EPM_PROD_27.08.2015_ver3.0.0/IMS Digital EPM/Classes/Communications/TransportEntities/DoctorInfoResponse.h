//
//  DoctorInfoResponse.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/04/13.
//
//

#import <Foundation/Foundation.h>

@interface DoctorInfoResponse : NSObject

@property (nonatomic, assign) NSInteger age;
@property (nonatomic, assign) NSInteger genderId;
@property (nonatomic, assign) NSInteger universityId;
@property (nonatomic, copy) NSString* manualUnivertityName;
@property (nonatomic, assign) NSInteger yearOfDegree;
@property (nonatomic, assign) NSInteger specializationId;
@property (nonatomic, retain) NSNumber* secondSpecializationId;
@property (nonatomic, assign) NSInteger countyId;
@property (nonatomic, assign, getter = isHealthCenter) BOOL healthCenter;
@property (nonatomic, assign, getter = isPublicHospital) BOOL publicHospital;
@property (nonatomic, assign, getter = isPrivateHospital) BOOL privateHospital;
@property (nonatomic, assign, getter = isConsultation) BOOL consultation;
@property (nonatomic, assign, getter = isOtherHealthCenter) BOOL otherHealthCenter;
@property (nonatomic, copy) NSString* otherHealthCenterText;
@property (nonatomic, assign) NSInteger weeklyHours;
@property (nonatomic, assign) NSInteger weeklyPatients;
@property (nonatomic, copy) NSString* comments;
@property (nonatomic, copy) NSString* email;

@end
