//
//  ChatSendQuestionRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "BaseRequest.h"
#import "ChatMessage.h"

@interface ChatSendQuestionRequest : BaseRequest

/**
 @brief Constructor for ChatSendQuestionRequest
 @param userName User id for login
 @param question Question to be sent
 @param onComplete Block that will be executed if login is successful
 @param onError Block that will be executed if login is not successful
 */
- (id)initChatSendQuestionRequestWithUserId:(NSString*)userId andChatMessage:(ChatMessage*)chatMessage onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end
