//
//  ChatGetAnswersRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import "BaseRequest.h"

/** Block that will be executed if login is successful */
typedef void(^ChatGetAnswersResponseBlock)(NSArray* answers);

@interface ChatGetAnswersRequest : BaseRequest

/**
 @brief Constructor for LoginRequest
 @param userName User id for login
 @param onComplete Block that will be executed if login is successful
 @param onError Block that will be executed if login is not successful
 */
- (id)initGetAnswersRequestWithUserId:(NSString*)userId onComplete:(ChatGetAnswersResponseBlock)onComplete onError:(RequestErrorBlock)onError;

@end
