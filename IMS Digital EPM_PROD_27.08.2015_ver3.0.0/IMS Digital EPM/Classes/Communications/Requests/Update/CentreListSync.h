//
//  CentreListSync.h
//  IMS Digital EPM
//
//  Created by Bukka, Ravi (Bangalore) on 04/08/14.
//
//

#import <Foundation/Foundation.h>

@interface CentreListSync : NSObject

/** @brief Identifier provided by remote server */
@property (nonatomic, retain) NSString *identifier;
/** @brief Centre description */
@property (nonatomic, retain) NSString *name;

@end
