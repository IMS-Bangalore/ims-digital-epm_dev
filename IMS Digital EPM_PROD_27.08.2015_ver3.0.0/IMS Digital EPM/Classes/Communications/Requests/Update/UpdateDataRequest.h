//
//  UpdateDataRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 30/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "UpdateDataResponse.h"
#import "DoctorInfo.h"


/** 
 @brief Block that will be called when UpdateDataRequest is successfuly completed
 @param response UpdateDataResponse object containing update info
 */
typedef void(^UpdateDataResponseBlock)(UpdateDataResponse* response, NSString* offset);

/** @brief Request to obtain application data updates */
@interface UpdateDataRequest : BaseRequest

/**
 @brief Creates a request to obtain application data updates
 @param syncType Defines the type of synchronization
 @param treatment Treatment info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initUpdateDataRequestWithDoctorInfo:(DoctorInfo*)doctorInfo andOffset:(NSString*)offset lastUpdate:(NSDate*)lastUpdate onComplete:(UpdateDataResponseBlock)onComplete onError:(RequestErrorBlock)onError;

@end
