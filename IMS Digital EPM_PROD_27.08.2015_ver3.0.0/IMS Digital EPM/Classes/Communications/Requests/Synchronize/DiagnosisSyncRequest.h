//
//  DiagnosisSyncRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "Diagnosis.h"

typedef enum {
    kDiagnosisSyncTypeInsert,
    kDiagnosisSyncTypeUpdate,
    kDiagnosisSyncTypeDelete
} DiagnosisSyncType;

/* @brief Request to synchronize local diagnosis info with remote server */
@interface DiagnosisSyncRequest : BaseRequest

/**
 @brief Creates a request to synchronize local diagnosis info with remote server
 @param syncType Defines the type of synchronization
 @param diagnosis Diagnosis info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initDiagnosisSyncRequestWithSyncType:(DiagnosisSyncType)syncType diagnosis:(Diagnosis*)diagnosis onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end