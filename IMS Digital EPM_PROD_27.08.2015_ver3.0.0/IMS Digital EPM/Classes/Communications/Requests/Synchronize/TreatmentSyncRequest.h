//
//  TreatmentSyncRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "Treatment.h"

typedef enum {
    kTreatmentSyncTypeInsert,
    kTreatmentSyncTypeUpdate,
    kTreatmentSyncTypeDelete
} TreatmentSyncType;

/* @brief Request to synchronize local treatment info with remote server */
@interface TreatmentSyncRequest : BaseRequest

/**
 @brief Creates a request to synchronize local treatment info with remote server
 @param syncType Defines the type of synchronization
 @param treatment Treatment info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initTreatmentSyncRequestWithSyncType:(TreatmentSyncType)syncType treatment:(Treatment*)treatment onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end