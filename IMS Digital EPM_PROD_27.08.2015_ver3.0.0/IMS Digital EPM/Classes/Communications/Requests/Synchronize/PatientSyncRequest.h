//
//  PatientSyncRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 28/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "BaseRequest.h"
#import "Patient.h"

typedef enum {
    kPatientSyncTypeInsert,
    kPatientSyncTypeUpdate,
    kPatientSyncTypeDelete
} PatientSyncType;

/* @brief Request to synchronize local patient info with remote server */
@interface PatientSyncRequest : BaseRequest

/**
 @brief Creates a request to synchronize local patient info with remote server
 @param syncType Defines the type of synchronization
 @param patient Patient info to synchronize
 @param onComplete Block that will be executed if request is successful
 @param onError Block that will be executed if request is not successful
 */
-(id) initPatientSyncRequestWithSyncType:(PatientSyncType)syncType patient:(Patient*)patient onComplete:(RequestSuccessBlock)onComplete onError:(RequestErrorBlock)onError;

@end
