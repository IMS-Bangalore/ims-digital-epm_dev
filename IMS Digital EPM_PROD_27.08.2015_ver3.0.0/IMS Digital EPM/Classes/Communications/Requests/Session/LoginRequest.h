//
//  LoginRequest.h
//  IMS Digital EPM
//
//  Created by Alex Guti on 26/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "IMSRequest.h"

/** Block that will be executed if login is successful */
typedef void(^LoginResponseBlock)(NSDate* firstCollaborationDate, NSDate* firstCollaborationDateEnd, NSDate* secondCollaborationDate, NSDate* secondCollaborationDateEnd, NSString* masterPassword);

/** @brief Request to perform a login into server*/
@interface LoginRequest : IMSRequest

/**
 @brief Constructor for LoginRequest
 @param userName User id for login
 @param password User Password
 @param removePreviousData Defines if previous data stored for the doctor should be removed
 @param onComplete Block that will be executed if login is successful
 @param onError Block that will be executed if login is not successful
 */
-(id) initLoginRequestWithUserName:(NSString*)userName password:(NSString*)password appVersion:appVersion removePreviousData:(BOOL)removePreviousData onComplete:(LoginResponseBlock)onComplete onError:(TCRequestErrorBlock)onError;

@end
