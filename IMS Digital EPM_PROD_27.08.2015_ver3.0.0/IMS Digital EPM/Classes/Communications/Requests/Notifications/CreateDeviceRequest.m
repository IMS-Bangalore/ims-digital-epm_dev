//
//  CreateDeviceRequest.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import "CreateDeviceRequest.h"
#import "NSDictionary+ArrayForKey.h"
#import "OpenUDID.h"

static NSString* const kDateFormat = @"yyyy-MM-dd HH:mm:ss";

/* Request info */
static NSString* const kResourceName = @"devices";

// Content dictionary
static NSString* const kTokenKey = @"token";
static NSString* const kDeviceAliasKey = @"alias_device";
static NSString* const kUDIDKey = @"udid";

/* Response parameters */
static NSString* const kResponseWrapper = @"objects";
static NSString* const kResponseDeviceIdKey = @"id";
static NSString* const kResponseTokenKey = @"token";
static NSString* const kResponseDeviceAliasKey = @"alias_device";
static NSString* const kResponseCreationDateKey = @"created_at";
static NSString* const kResponseUpdateDateKey = @"updated_at";
static NSString* const kResponseLastRegistrationDateKey = @"last_registered_at";
static NSString* const kResponseAppIdKey = @"app_id";
static NSString* const kResponseTypeKey = @"type";

@implementation CreateDeviceRequest

- (id)initCreateDeviceRequestWithToken:(NSString*)token andDeviceAlias:(NSString*)deviceAlias onComplete:(CreateDeviceResponseBlock)onComplete onError:(RequestErrorBlock)onError {
    self = [super init];
    if (self) {
        //self.segmentParams = [NSArray arrayWithObjects:@"device", deviceId, nil];
//Ravi_Bukka: Commented for APNS
//        self.requestMethod = kTCRequestMethodPOST;
//        // Set resource name
//        self.resource = kResourceName;
//        // Add request content
//        if (token != nil) {
//            [self addParam:token forKey:kTokenKey];
//        }
//        if (deviceAlias != nil) {
//            [self addParam:deviceAlias forKey:kDeviceAliasKey];
//        }
//        [self addParam:[OpenUDID value] forKey:kUDIDKey];
        
//Ravi_Bukka: Added new data for APNS
        
        UIDevice* device = [UIDevice currentDevice];
        NSUUID *deviceID = device.identifierForVendor;
        NSString *deviceIDStr = deviceID.UUIDString;
        deviceIDStr = [deviceIDStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        self.requestMethod = kTCRequestMethodPUT;
        //Deepak Carpenter: Register Device on Server
        

        [self addParam:deviceIDStr forKey:@"deviceID"];
        [self addParam:deviceAlias forKey:@"doctorID"];
        [self addParam:token forKey:@"regID"];
        [self addParam:@"0" forKey:@"sandbox"];

        // Set response handler blocks
        self.onError = onError;
        self.onComplete = ^(NSDictionary* responseDictionary) {
            Device* device = nil;
            device = [self deviceFromDictionary:responseDictionary];
            onComplete(device);
        };
    }
    return self;
}

- (Device*)deviceFromDictionary:(NSDictionary*)dictionary {
    NSArray* deviceArray = [dictionary arrayForKey:kResponseWrapper];
    Device* device = nil;
    if (deviceArray.count > 0) {
        device = [[[Device alloc] init] autorelease];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:kDateFormat];
        
        NSDictionary* deviceDict = deviceArray[0];
        device.deviceId = [[deviceDict objectForKey:kResponseDeviceIdKey] stringValue];
        device.token = [deviceDict objectForKey:kResponseTokenKey];
        device.deviceAlias = [deviceDict objectForKey:kResponseDeviceAliasKey];
        device.creationDate = [dateFormatter dateFromString:[deviceDict objectForKey:kResponseCreationDateKey]];
        device.updateDate = [dateFormatter dateFromString:[deviceDict objectForKey:kResponseUpdateDateKey]];
        device.lastRegistrationDate = [dateFormatter dateFromString:[deviceDict objectForKey:kResponseLastRegistrationDateKey]];
        device.appId = [[deviceDict objectForKey:kResponseAppIdKey] stringValue];
        device.type = [deviceDict objectForKey:kResponseTypeKey];
        [dateFormatter release];
    }
    return device;
}

@end
