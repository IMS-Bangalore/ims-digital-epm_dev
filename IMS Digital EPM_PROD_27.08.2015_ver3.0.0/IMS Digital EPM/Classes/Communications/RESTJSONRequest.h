//
//  RESTJSONRequest.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 13/12/12.
//
//

#import <Foundation/Foundation.h>
#import "TwinCodersLibrary.h"

@interface RESTJSONRequest : TCRESTJSONRequest

@property (nonatomic, strong) NSString* method;

@end
