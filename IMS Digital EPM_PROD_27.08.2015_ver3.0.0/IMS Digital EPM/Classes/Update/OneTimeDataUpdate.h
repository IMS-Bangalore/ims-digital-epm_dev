//
//  OneTimeDataUpdate.h
//  IMS Digital EPM
//
//  Created by Daniel Deneka on 16/03/2020.
//
//

#import <Foundation/Foundation.h>

@interface OneTimeDataUpdate : NSObject

@property (nonatomic, assign) BOOL shouldPerformUpdate;
@property (nonatomic, copy, readonly) NSArray<NSString *> *diagnosesIds;
@property (nonatomic, copy, readonly) NSArray<NSString *> *productsIds;

@end
