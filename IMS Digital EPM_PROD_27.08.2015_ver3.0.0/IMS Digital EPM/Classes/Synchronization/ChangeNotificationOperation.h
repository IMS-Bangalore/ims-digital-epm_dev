//
//  ChangeNotificationOperation.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 29/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChangeNotificationOperation;
@protocol ChangeNotificationOperationDelegate <NSObject>
- (void)changeNotificationOperation:(ChangeNotificationOperation*)operation didFailWithError:(NSError*)error;
- (void)changeNotificationOperationDidComplete:(ChangeNotificationOperation*)operation;
@end

#pragma mark - Typedefs
/** @brief Block that will be given for objects that will handle request response, in order to handle request errors */
typedef void(^ChangeNotificationOperationErrorBlock)(NSError* error);

/**
 @brief Block to be called when a request is completed successfuly without response data */
typedef void(^ChangeNotificationOperationSuccessBlock)();

typedef enum {
    kChangeNotificationOperationAdd,
    kChangeNotificationOperationUpdate,
    kChangeNotificationOperationDelete
} ChangeNotificationOperationType;

@interface ChangeNotificationOperation : NSOperation

#pragma mark - Properties
@property (nonatomic, readonly) NSManagedObjectID* syncObjectID;
@property (nonatomic, readonly) NSManagedObjectID* syncActionID;
@property (nonatomic, readonly) ChangeNotificationOperationType operationType;
@property (nonatomic, assign) id<ChangeNotificationOperationDelegate> delegate;
@property (nonatomic, copy) ChangeNotificationOperationErrorBlock errorBlock;
@property (nonatomic, copy) ChangeNotificationOperationSuccessBlock successBlock;


#pragma mark - Methods
- (id)initWithObjectID:(NSManagedObjectID*)syncObjectID operationType:(ChangeNotificationOperationType)operationType syncActionID:(NSManagedObjectID*)syncActionID delegate:(id<ChangeNotificationOperationDelegate>)delegate;

- (id)initWithObjectID:(NSManagedObjectID*)syncObjectID operationType:(ChangeNotificationOperationType)operationType syncActionID:(NSManagedObjectID*)syncActionID successBlock:(ChangeNotificationOperationSuccessBlock)successBlock errorBlock:(ChangeNotificationOperationErrorBlock)errorBlock;

@end
