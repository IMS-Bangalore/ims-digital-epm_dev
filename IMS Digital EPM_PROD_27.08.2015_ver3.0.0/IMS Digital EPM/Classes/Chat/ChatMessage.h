//
//  ChatMessage.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 05/12/12.
//
//

#import <Foundation/Foundation.h>

@interface ChatMessage : NSObject

@property (nonatomic, retain) NSDate* date;
@property (nonatomic, copy) NSString* message;
@property (nonatomic, getter = isQuestion) BOOL question;


@end
