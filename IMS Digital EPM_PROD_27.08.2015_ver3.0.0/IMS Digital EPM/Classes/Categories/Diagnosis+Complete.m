//
//  Diagnosis+Complete.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 15/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Diagnosis+Complete.h"
#import "Treatment+Complete.h"

@implementation Diagnosis (Complete)
- (BOOL)isComplete {
    BOOL isComplete = YES;
    
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])
    {
        isComplete = isComplete && (self.diagnosisType != nil || self.userDiagnosis.length > 0);
        isComplete = isComplete && self.visitType != nil;
       if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"]) {
        // isComplete = isComplete && self.pathology != nil;
       }
        else
            isComplete = isComplete && self.pathology != nil;

        //Kanchan
        isComplete =isComplete && (self.drugIndicator !=nil || self.userDrugIndicator.length > 0);

        if (self.needsTreatment.boolValue) {
            isComplete = isComplete && self.treatments.count > 0;
            for (Treatment* treatment in self.treatments) {
                isComplete = isComplete && [treatment isComplete];
            }
        }
 
    }
    else
    {
    isComplete = isComplete && (self.diagnosisType != nil || self.userDiagnosis.length > 0);
    isComplete = isComplete && self.visitType != nil;
    isComplete = isComplete && self.pathology != nil;
    if (self.needsTreatment.boolValue) {
        isComplete = isComplete && self.treatments.count > 0;
        for (Treatment* treatment in self.treatments) {
            isComplete = isComplete && [treatment isComplete];
        }
     }
    }
    return isComplete;
}

- (BOOL)isEmpty {
    BOOL isEmpty = YES;
    if ([[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] isEqualToString:@"pl"])

    {
        isEmpty = isEmpty && (self.diagnosisType == nil && self.userDiagnosis.length == 0);
        isEmpty = isEmpty && self.visitType == nil;
        isEmpty = isEmpty && self.pathology == nil;
        //Kanchan
        isEmpty =isEmpty && (self.drugIndicator ==nil && self.userDrugIndicator.length == 0);
        isEmpty = isEmpty && self.needsTreatment.boolValue;
        isEmpty = isEmpty && self.treatments.count <= 1;
        for (Treatment* treatment in self.treatments) {
            isEmpty = isEmpty && [treatment isEmpty];
        }
    }
    
    else{
    isEmpty = isEmpty && (self.diagnosisType == nil && self.userDiagnosis.length == 0);
    isEmpty = isEmpty && self.visitType == nil;
    isEmpty = isEmpty && self.pathology == nil;
    isEmpty = isEmpty && self.needsTreatment.boolValue;
    isEmpty = isEmpty && self.treatments.count <= 1;
    for (Treatment* treatment in self.treatments) {
        isEmpty = isEmpty && [treatment isEmpty];
    }
    }
    return isEmpty;
}
@end
