//
//  Diagnosis+SortedTreatments.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Diagnosis+SortedTreatments.h"

@implementation Diagnosis (SortedTreatments)
- (NSArray*)sortedTreatments {
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    return [self.treatments sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
}
@end
