//
//  DoctorInfo+SortedPatients.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "DoctorInfo+SortedPatients.h"

@implementation DoctorInfo (SortedPatients)
- (NSArray*)sortedPatients {
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    return [self.patients sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
}
@end
