//
//  Diagnosis+SortedTreatments.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Diagnosis.h"

@interface Diagnosis (SortedTreatments)
- (NSArray*)sortedTreatments;
@end
