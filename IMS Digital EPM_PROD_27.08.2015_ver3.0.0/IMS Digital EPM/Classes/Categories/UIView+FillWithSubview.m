//
//  UIView+FillWithSubview.m
//  IMS Digital EPM
//
//  Created by Alex Guti on 09/03/12.
//  Copyright (c) 2012 TwinCoders S.L. All rights reserved.
//

#import "UIView+FillWithSubview.h"

@implementation UIView (FillWithSubview)

-(void) fillWithSubview:(UIView *)subview {
    CGRect frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    subview.frame = frame;
    [self addSubview:subview];
    self.backgroundColor = [UIColor clearColor];
}

@end
