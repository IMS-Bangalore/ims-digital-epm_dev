//
//  Notification.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 12/12/12.
//
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject

@property (nonatomic, copy) NSString* descriptionText;
@property (nonatomic, retain) NSDate* date;
@property (nonatomic, copy) NSString* contentUrl;
@property (nonatomic, copy) NSNumber* notificationId;
@property (nonatomic, copy) NSString* type;

@end
