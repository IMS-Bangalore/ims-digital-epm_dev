//
//  MetaInfo.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 09/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MetaInfo : NSManagedObject

@property (nonatomic, retain) NSString * databaseVersion;
@property (nonatomic, retain) NSDate * lastUpdate;
@property (nonatomic, retain) NSString * initialLoadVersion;

@end
