//
//  Pathology.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 21/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Pathology.h"
#import "Diagnosis.h"


@implementation Pathology

@dynamic identifier;
@dynamic name;
@dynamic diagnostics;

@end
