//
//  OtherSickFund.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface OtherSickFund : NSManagedObject

@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) Patient *patient;

@end
