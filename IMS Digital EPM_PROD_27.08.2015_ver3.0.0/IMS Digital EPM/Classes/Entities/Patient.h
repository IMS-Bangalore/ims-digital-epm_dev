//
//  Patient.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Synchronizable.h"

typedef NS_ENUM(NSInteger, TypeOfContact) {
    TypeOfContactUndefined = 0,
    TypeOfContactInPerson,
    TypeOfContactInRemote,
    TypeOfContactCount
};

@class Age, ConsultType, Diagnosis, DoctorInfo, Gender, Insurance, PlaceOfVisit, Recurrence, SickFund, Smoker;

@interface Patient : Synchronizable

@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSNumber * recurranceValue;
@property (nonatomic, retain) NSNumber * sickFundValue;
@property (nonatomic, retain) NSString * userInsurance;
@property (nonatomic, retain) NSString * userPlaceOfVisit;
@property (nonatomic, retain) NSString * userSickFund;
@property (nonatomic, retain) NSDate * visitDate;
@property (nonatomic, retain) NSString * otherSickFundValue;
@property (nonatomic, retain) NSNumber *typeOfContact;
@property (nonatomic, retain) Age *age;
@property (nonatomic, retain) ConsultType *consultType;
@property (nonatomic, retain) NSSet *diagnostic;
@property (nonatomic, retain) DoctorInfo *doctorInfo;
@property (nonatomic, retain) Gender *gender;
@property (nonatomic, retain) Insurance *insurance;
@property (nonatomic, retain) PlaceOfVisit *placeOfVisit;
@property (nonatomic, retain) Recurrence *recurrence;
@property (nonatomic, retain) SickFund *sickFund;
@property (nonatomic, retain) Smoker *smoker;
@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addDiagnosticObject:(Diagnosis *)value;
- (void)removeDiagnosticObject:(Diagnosis *)value;
- (void)addDiagnostic:(NSSet *)values;
- (void)removeDiagnostic:(NSSet *)values;

@end

@interface Patient (Enums)

- (TypeOfContact)typeOfContactEnum;
- (void)setTypeOfContactEnum:(TypeOfContact)typeOfContactEnum;

@end
