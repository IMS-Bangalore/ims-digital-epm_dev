//
//  Synchronizable.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 02/04/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SyncAction, Synchronizable;

@interface Synchronizable : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet *actions;
@property (nonatomic, retain) NSSet *secondaryTargetActions;
@property (nonatomic, retain) Synchronizable *parentEntity;
@property (nonatomic, retain) NSSet *childEntities;
@end

@interface Synchronizable (CoreDataGeneratedAccessors)

- (void)addActionsObject:(SyncAction *)value;
- (void)removeActionsObject:(SyncAction *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;
- (void)addSecondaryTargetActionsObject:(SyncAction *)value;
- (void)removeSecondaryTargetActionsObject:(SyncAction *)value;
- (void)addSecondaryTargetActions:(NSSet *)values;
- (void)removeSecondaryTargetActions:(NSSet *)values;
- (void)addChildEntitiesObject:(Synchronizable *)value;
- (void)removeChildEntitiesObject:(Synchronizable *)value;
- (void)addChildEntities:(NSSet *)values;
- (void)removeChildEntities:(NSSet *)values;
@end
