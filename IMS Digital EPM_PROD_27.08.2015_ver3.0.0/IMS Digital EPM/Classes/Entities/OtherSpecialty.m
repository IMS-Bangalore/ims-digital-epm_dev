//
//  OtherSpecialty.m
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 15/12/14.
//
//

#import "OtherSpecialty.h"
#import "Doctor.h"
#import "Treatment.h"


@implementation OtherSpecialty

@dynamic identifier;
@dynamic name;
@dynamic treatments;
@dynamic secondarySpeciality;

@end
