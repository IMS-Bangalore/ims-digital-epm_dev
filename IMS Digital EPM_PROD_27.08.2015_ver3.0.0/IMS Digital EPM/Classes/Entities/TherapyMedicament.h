//
//  TherapyMedicament.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 11/03/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface TherapyMedicament : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *treatmentMedicament;
@end

@interface TherapyMedicament (CoreDataGeneratedAccessors)

- (void)addTreatmentMedicamentObject:(Treatment *)value;
- (void)removeTreatmentMedicamentObject:(Treatment *)value;
- (void)addTreatmentMedicament:(NSSet *)values;
- (void)removeTreatmentMedicament:(NSSet *)values;

@end
