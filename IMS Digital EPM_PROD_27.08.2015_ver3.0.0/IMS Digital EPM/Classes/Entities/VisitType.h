//
//  VisitType.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 21/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Diagnosis;

@interface VisitType : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *diagnostics;
@end

@interface VisitType (CoreDataGeneratedAccessors)

- (void)addDiagnosticsObject:(Diagnosis *)value;
- (void)removeDiagnosticsObject:(Diagnosis *)value;
- (void)addDiagnostics:(NSSet *)values;
- (void)removeDiagnostics:(NSSet *)values;
@end
