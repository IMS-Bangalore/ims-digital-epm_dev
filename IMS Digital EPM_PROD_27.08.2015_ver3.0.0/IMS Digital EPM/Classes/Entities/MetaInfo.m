//
//  MetaInfo.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 09/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "MetaInfo.h"


@implementation MetaInfo

@dynamic databaseVersion;
@dynamic lastUpdate;
@dynamic initialLoadVersion;

@end
