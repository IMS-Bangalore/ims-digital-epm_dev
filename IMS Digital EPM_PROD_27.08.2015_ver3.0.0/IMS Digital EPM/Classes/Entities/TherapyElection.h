//
//  TherapyElection.h
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 25/08/14.
//
//  Replaced on 25 aug to change relation between treatment & This entity

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Treatment;

@interface TherapyElection : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *therapyElectionTreatment;
@end

@interface TherapyElection (CoreDataGeneratedAccessors)

- (void)addTherapyElectionTreatmentObject:(Treatment *)value;
- (void)removeTherapyElectionTreatmentObject:(Treatment *)value;
- (void)addTherapyElectionTreatment:(NSSet *)values;
- (void)removeTherapyElectionTreatment:(NSSet *)values;

@end
