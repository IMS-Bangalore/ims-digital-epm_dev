//
//  University.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "University.h"
#import "Doctor.h"


@implementation University

@dynamic name;
@dynamic userDefined;
@dynamic identifier;
@dynamic doctors;

@end
