//
//  TherapyElection.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 25/08/14.
//
//

#import "TherapyElection.h"
#import "Treatment.h"


@implementation TherapyElection

@dynamic identifier;
@dynamic name;
@dynamic therapyElectionTreatment;

@end
