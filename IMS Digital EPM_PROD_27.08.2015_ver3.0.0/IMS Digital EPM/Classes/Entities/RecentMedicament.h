//
//  RecentMedicament.h
//  IMS Digital EPM
//
//  Created by Diego Prados on 15/10/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DoctorInfo, Medicament;

@interface RecentMedicament : NSManagedObject

@property (nonatomic, retain) NSNumber * count;
@property (nonatomic, retain) NSDate * lastUse;
@property (nonatomic, retain) DoctorInfo *doctorInfo;
@property (nonatomic, retain) Medicament *medicament;

@end
