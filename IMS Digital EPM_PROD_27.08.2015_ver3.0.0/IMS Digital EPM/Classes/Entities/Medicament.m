//
//  Medicament.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 13/03/14.
//
//

#import "Medicament.h"
#import "Presentation.h"
#import "ProductType.h"
#import "RecentMedicament.h"
#import "Treatment.h"


@implementation Medicament

@dynamic deleted;
@dynamic identifier;
@dynamic name;
@dynamic presentations;
@dynamic productType;
@dynamic recentUses;
@dynamic replacedInTreatments;
@dynamic treatments;

@end
