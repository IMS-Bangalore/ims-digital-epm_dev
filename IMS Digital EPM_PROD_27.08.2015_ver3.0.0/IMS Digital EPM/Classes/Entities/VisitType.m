//
//  VisitType.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 21/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "VisitType.h"
#import "Diagnosis.h"


@implementation VisitType

@dynamic identifier;
@dynamic name;
@dynamic diagnostics;

@end
