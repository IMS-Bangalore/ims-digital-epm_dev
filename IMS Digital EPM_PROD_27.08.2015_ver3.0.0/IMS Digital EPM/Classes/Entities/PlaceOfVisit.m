//
//  PlaceOfVisit.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 10/02/14.
//
//

#import "PlaceOfVisit.h"
#import "Patient.h"


@implementation PlaceOfVisit

@dynamic identifier;
@dynamic name;
@dynamic deleted;
@dynamic patient;

@end
