//
//  DurationType.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "DurationType.h"
#import "Dosage.h"


@implementation DurationType

@dynamic name;
@dynamic identifier;
@dynamic dosages;

@end
