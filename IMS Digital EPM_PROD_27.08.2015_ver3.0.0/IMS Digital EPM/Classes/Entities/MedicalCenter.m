//
//  MedicalCenter.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 21/02/14.
//
//

#import "MedicalCenter.h"
#import "Doctor.h"
#import "DoctorInfo.h"


@implementation MedicalCenter

@dynamic identifier;
@dynamic name;
@dynamic userDefined;
@dynamic doctorInfos;
@dynamic doctors;

@end
