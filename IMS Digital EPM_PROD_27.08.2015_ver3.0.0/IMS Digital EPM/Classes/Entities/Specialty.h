//
//  Specialty.h
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Doctor;

@interface Specialty : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSSet *mainSpecialityDoctors;
@property (nonatomic, retain) Doctor *secondarySpecialityDoctors;
@end

@interface Specialty (CoreDataGeneratedAccessors)

- (void)addMainSpecialityDoctorsObject:(Doctor *)value;
- (void)removeMainSpecialityDoctorsObject:(Doctor *)value;
- (void)addMainSpecialityDoctors:(NSSet *)values;
- (void)removeMainSpecialityDoctors:(NSSet *)values;
@end
