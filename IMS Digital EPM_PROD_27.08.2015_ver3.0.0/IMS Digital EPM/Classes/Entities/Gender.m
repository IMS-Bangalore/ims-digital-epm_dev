//
//  Gender.m
//  IMS Digital EPM
//
//  Created by Guillermo Gutiérrez on 08/03/12.
//  Copyright (c) 2012 TwinCoders. All rights reserved.
//

#import "Gender.h"
#import "Doctor.h"
#import "Patient.h"


@implementation Gender

@dynamic name;
@dynamic identifier;
@dynamic doctors;
@dynamic patients;

@end
