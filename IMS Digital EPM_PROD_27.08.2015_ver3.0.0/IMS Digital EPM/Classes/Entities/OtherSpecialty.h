//
//  OtherSpecialty.h
//  IMS Digital EPM
//
//  Created by Nair, Kanchan (Bangalore) on 15/12/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Doctor, Treatment;

@interface OtherSpecialty : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *treatments;
@property (nonatomic, retain) NSSet *secondarySpeciality;
@end

@interface OtherSpecialty (CoreDataGeneratedAccessors)

- (void)addTreatmentsObject:(Treatment *)value;
- (void)removeTreatmentsObject:(Treatment *)value;
- (void)addTreatments:(NSSet *)values;
- (void)removeTreatments:(NSSet *)values;

- (void)addSecondarySpecialityObject:(Doctor *)value;
- (void)removeSecondarySpecialityObject:(Doctor *)value;
- (void)addSecondarySpeciality:(NSSet *)values;
- (void)removeSecondarySpeciality:(NSSet *)values;

@end
