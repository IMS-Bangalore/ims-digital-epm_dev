//
//  SickFund.m
//  IMS Digital EPM
//
//  Created by Deepak Carpenter on 24/02/14.
//
//

#import "SickFund.h"
#import "Patient.h"


@implementation SickFund

@dynamic identifier;
@dynamic name;
@dynamic patients;

@end
