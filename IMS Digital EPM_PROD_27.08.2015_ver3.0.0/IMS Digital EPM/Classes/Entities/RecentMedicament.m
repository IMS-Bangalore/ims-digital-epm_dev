//
//  RecentMedicament.m
//  IMS Digital EPM
//
//  Created by Diego Prados on 15/10/12.
//
//

#import "RecentMedicament.h"
#import "DoctorInfo.h"
#import "Medicament.h"


@implementation RecentMedicament

@dynamic count;
@dynamic lastUse;
@dynamic doctorInfo;
@dynamic medicament;

@end
